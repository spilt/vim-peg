" Language:    PEG
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

if exists("b:did_indent")
  finish
endif

let b:did_indent = 1

setlocal autoindent
setlocal indentexpr=GetPEGIndent()
setlocal indentkeys+=-:

" Only define the function once.
if exists("*GetPEGIndent")
  finish
endif

function! GetPEGIndent()
  let line = getline(v:lnum)
  let current_ind = indent(v:lnum)
  let previousNum = prevnonblank(v:lnum - 1)
  let previous = getline(previousNum)
  let ind = indent(previousNum)

  if previous =~ '<-\s*\($\|--\)' || previous =~ ':\s*\($\|--\)'
    let ind = &tabstop
  endif

  if line =~ '^\s*\w\+\s*<-\|^\s*\w\+\s*(\s*\w+\s*)\s*:'
    let ind = 0
  endif

  if ind == indent(previousNum)
      return current_ind
  endif
  return ind
endfunction
