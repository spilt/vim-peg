This is a vim plugin for Parsing Expression Grammars, using the [LPEG "re" syntax](http://www.inf.puc-rio.br/~roberto/lpeg/re.html).
I recommend installing it with [vim-plug](https://github.com/junegunn/vim-plug):

```
Plug 'https://bitbucket.org/spilt/vim-peg', { 'for': 'peg' }
```
