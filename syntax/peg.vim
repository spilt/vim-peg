" Language:    PEG
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

" Bail if our syntax is already loaded.
if exists('b:current_syntax') && b:current_syntax == 'peg'
  finish
endif

syn region PEGComment start=/--/ end=/$/
hi def link PEGComment Comment
hi PEGComment ctermfg=DarkBlue

syn region PEGString start=/\z(["']\)/ end=/\z1/
hi def link PEGString String

syn region PEGGroup matchgroup=PEGGroupBrackets start=/\[/ end=/]/ contains=PEGExternal,PEGNegateGroup,PEGGroupRange
hi PEGGroup ctermfg=Magenta
hi PEGGroupBrackets ctermfg=LightMagenta

syn match PEGNegateGroup ;\[\@<=\^; contained
hi PEGNegateGroup ctermfg=Gray

syn match PEGGroupRange ;\-]\@!; contained
hi PEGGroupRange ctermfg=Gray

syn match PEGOperator ;[+*?&!]\|->\|\^[+-]\?\d\+; contains=PEGNumber
hi PEGOperator ctermfg=White

syn match PEGSlash ;/;
hi PEGSlash ctermfg=DarkGray

syn match PEGNumber /[+-]\?\d\+/ contained
hi PEGNumber ctermfg=Red

syn match PEGDot /\./
hi PEGDot ctermfg=LightYellow cterm=bold

syn region PEGTable start=/{|/ end=/|}/ contains=@PEGAll
hi PEGTable ctermfg=Green

syn region PEGParens start=/(/ end=/)/ contains=@PEGAll
hi PEGParens ctermfg=Yellow

syn region PEGNamedCapture matchgroup=PEGNamedCapture start=/{:\w\+:/rs=e end=/:}/ contains=@PEGAll
hi PEGNamedCapture ctermfg=LightBlue

syn region PEGSub start=/{\~/ end=/\~}/ contains=@PEGAll
hi PEGSub ctermfg=DarkRed

syn region PEGCapture start=/{[:|~]\@!/ end=/[:|~]\@<!}/ contains=@PEGAll
hi PEGCapture ctermfg=LightBlue

syn match PEGName /\w\+/
hi PEGName ctermfg=none

syn match PEGExternal /%\w\+/
hi PEGExternal ctermfg=Cyan

syn match PEGBackref /=\w\+/
hi PEGBackref ctermfg=LightBlue

syn match PEGDef /\w\+\(\s*(\s*\w\+\s*)\)\?\s*<-/ contains=PEGDefName
hi PEGDef ctermfg=White cterm=bold

syn match PEGDefName /(\s*\w\+\s*)/ contained
hi PEGDefName ctermfg=Gray

syn cluster PEGAll contains=PEGComment,PEGString,PEGGroup,PEGOperator,PEGSlash,PEGTable,PEGParens,
            \PEGNamedCapture,PEGCapture,PEGName,PEGExternal,PEGBackref,PEGDef,PEGDot

if !exists('b:current_syntax')
  let b:current_syntax = 'peg'
endif
