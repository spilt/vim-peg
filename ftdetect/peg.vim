" Language:    PEG
" Maintainer:  Bruce Hill <bruce@bruce-hill.com>
" License:     WTFPL

autocmd BufNewFile,BufRead *.peg set filetype=peg
